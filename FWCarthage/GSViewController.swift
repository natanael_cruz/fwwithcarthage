//
//  GSViewController.swift
//  FWCarthage
//
//  Created by 961156 on 6/1/20.
//  Copyright © 2020 961156. All rights reserved.
//

import UIKit

open class GSViewController: UIViewController {
    public func changeBackground(withColor color: UIColor) {
        self.view.backgroundColor = color
    }
}
